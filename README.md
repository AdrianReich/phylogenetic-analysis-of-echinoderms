#This repository has four functions:#

1. Host the script that will generate a semi annotated transcriptome from the submitted transcriptomes to NCBI Transcriptome Shotgun Assembly (TSA)
2. Host other scripts used in the analysis of the paper
3. Host the run statistics for the *de novo* assemblies
4. Link to the data hosted at external locations (NCBI and Data Dryad Repository)

##Publication##
Reich A., Dunn C.W., Akasaka K., Wessel, G.M. 2015. Phylogenomic Analyses of Echinodermata support the sister groups of Asterozoa and Echinozoa. PLoS One. 10(3) (Epub ahead of print).

Link to manuscript at [NCBI](http://www.ncbi.nlm.nih.gov/pubmed/25794146) and [PLoS One](http://journals.plos.org/plosone/article?id=10.1371/journal.pone.0119627)

###1. Generate a semi annotated transcriptome###

* Click on link for transcriptome of interest (see Table 1)
* Download and unzip the fsa_nt.gz file
* Download and unzip the Master_accs.gz file from [this repository](https://bitbucket.org/AdrianReich/phylogenetic-analysis-of-echinoderms/downloads)
* Run AnnotateTSAfasta.py script [(found here)](https://bitbucket.org/AdrianReich/phylogenetic-analysis-of-echinoderms/src)

###2. Scripts used in analyses###

* [Find them here](https://bitbucket.org/AdrianReich/phylogenetic-analysis-of-echinoderms/downloads)

###3. Agalma resource reports for assemblies###

* [This is the tabular report output of Agalma](http://adrianreich.bitbucket.org), which includes the run statistics and resource reports for the individual *de novo* assemblies (follow internal links).

###4. Data hosted at external sites###

* [NCBI BioProject](http://www.ncbi.nlm.nih.gov/bioproject/PRJNA236087) which includes links to the raw reads in the Short Read Archive
* [Data Dryad Repository](http://dx.doi.org/10.5061/dryad.22sf3) which includes the constraint trees and supermatrix alignments in phylip format

---

##Table 1
|Species name with link to NCBI TSA|Taxonomy ID with link to NCBI Taxonomy|Ovary Transcriptome Accession Prefix|
|---------------------------|:-----------:|:-----------:|
|[Leptasterias sp.](http://www.ncbi.nlm.nih.gov/Traces/wgs/?val=GAVC01)|[1462732](http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?id=1462732)|GAVC|
|[Henricia sp.](http://www.ncbi.nlm.nih.gov/Traces/wgs/?val=GAVP01)|[1462731](http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?id=1462731)|GAVP|
|[Echinaster spinulosus](http://www.ncbi.nlm.nih.gov/Traces/wgs/?val=GAVE01)|[1451296](http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?id=1451296)|GAVE|
|[Apostichopus japonicus](http://www.ncbi.nlm.nih.gov/Traces/wgs/?val=GAVS01)|[307972](http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?id=307972)|GAVS|
|[Lytechinus variegatus](http://www.ncbi.nlm.nih.gov/Traces/wgs/?val=GAUR01)|[7654](http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?id=7654)|GAUR|
|[Sphaerechinus granularis](http://www.ncbi.nlm.nih.gov/Traces/wgs/?val=GAVR01)|[39374](http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?id=39374)|GAVR|
|[Asterias amurensis](http://www.ncbi.nlm.nih.gov/Traces/wgs/?val=GAVJ01)|[7602](http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?id=7602)|GAVJ|
|[Asterina pectinifera](http://www.ncbi.nlm.nih.gov/Traces/wgs/?val=GAVK01)|[7594](http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?id=7594)|GAVK|
|[Parastichopus californicus](http://www.ncbi.nlm.nih.gov/Traces/wgs/?val=GAVO01)|[7689](http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?id=7689)|GAVO|
|[Pisaster ochraceus](http://www.ncbi.nlm.nih.gov/Traces/wgs/?val=GAVN01)|[7612](http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?id=7612)|GAVN|
|[Marthasterias glacialis](http://www.ncbi.nlm.nih.gov/Traces/wgs/?val=GAVI01)|[7609](http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?id=7609)|GAVI|
|[Luidia clathrata](http://www.ncbi.nlm.nih.gov/Traces/wgs/?val=GAVQ01)|[133437](http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?id=133437)|GAVQ|
|[Echinarachnius parma](http://www.ncbi.nlm.nih.gov/Traces/wgs/?val=GAVF01)|[869203](http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?id=869203)|GAVF|
|[Asterias rubens](http://www.ncbi.nlm.nih.gov/Traces/wgs/?val=GAUU01)|[7604](http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?id=7604)|GAUU|
|[Sclerodactyla briareus](http://www.ncbi.nlm.nih.gov/Traces/wgs/?val=GAUT01)|[7710](http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?id=7710)|GAUT|
|[Asterias forbesi](http://www.ncbi.nlm.nih.gov/Traces/wgs/?val=GAUS01)|[7603](http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?id=7603)|GAUS|
|[Ophiocoma echinata](http://www.ncbi.nlm.nih.gov/Traces/wgs/?val=GAUQ01)|[331088](http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?id=331088)|GAUQ|
|[Oxycomanthus japonicus](http://www.ncbi.nlm.nih.gov/Traces/wgs/?val=GAZO01)|[693457](http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?id=693457)|GAZO|
|[Patiria miniata](http://www.ncbi.nlm.nih.gov/Traces/wgs/?val=GAWB01)|[46514](http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?id=46514)|GAWB|
|[Eucidaris tribuloides](http://www.ncbi.nlm.nih.gov/Traces/wgs/?val=GAZP01)|[7632](http://www.ncbi.nlm.nih.gov/Taxonomy/Browser/wwwtax.cgi?id=7632)|GAZP|