import sys

Usage = """
Usage: AnnotateTSAfasta.py This script will read one or all TSA fasta files from the Reich et al 2014 transcriptomes and annotate the sequences that were assigned annotations.
To customize which values you would like output, please change the Boolean True/False variables in the script, the default is all fields, the minimum output is NCBI identifiers, species, and annotation name.

To annotate a single transcriptome:
1) Download the file from the TSA (links in Table 1 here: https://bitbucket.org/AdrianReich/phylogenetic-analysis-of-echinoderms/overview).
2) Unzip the files
3) Use the Accession Prefix as input (see Table 1 in link above e.g. GAUR for Lytechinus variegatus)
The input commands should be as follows: python AnnotateTSAfasta.py AccessionPrefix

To annotate all 20 transcriptomes:
1) Download all 20 files from the TSA (links in Table 1 here: https://bitbucket.org/AdrianReich/phylogenetic-analysis-of-echinoderms/overview).
2) Unzip the files
3) Execute the script with "all"
The input commands should be as follows: python AnnotateTSAfasta.py all
"""

Bool_REALid=True	#REALid = Reich Et AL id
Bool_Length=True	#Length of transcript
Bool_FPKM=True		#Abundance of transcript in ovary
Bool_SwissProt=True	#SwissProt ID
Bool_Blast=True		#blast hit of this sequence to SwissProt sequence

def FileLen(FileName):
	"Count the number of lines in the Master_accs file."
	with open(FileName, 'r') as f:
		for i, l in enumerate(f, start=1):
			pass
	return i

def FileOpen(FastaFileName):
	"Test if the fasta file can be opened."
	try:
		FastaFileRead=open(FastaFileName, 'r')
	except IOError:
		print FastaFileName,'is missing. Please confirm that you downloaded the file, unzipped it and put it in the current directory.'
		Exit=True
	else:
		Exit=False
		FastaFileRead.close()
	return Exit
	
def FastaSeqCount(FastaFileName, SeqsInCompleteFile):
	"Test if the fasta file(s) are complete."
	FastaRead = open(FastaFileName,'r')
	SeqCount=0
	Exit=False
        FaLine=FastaRead.readline().strip()
        while FaLine != '':
        	if FaLine[0]=='>':
        		SeqCount+=1
        	FaLine=FastaRead.readline().strip()
        if SeqCount!=SeqsInCompleteFile:
        	print FastaFileName,'is incorrect. The total number of sequences in this file should be',str(SeqsInCompleteFile)+'. You have',str(SeqCount)
        	print 'Please re-download the file and try again.'
        	Exit=True
        FastaRead.close()
	return Exit
	
if len(sys.argv) != 2:
	print Usage
else:
	try:	#Begin Master_accs presence verification and file length check
		AccsRead = open('Master_accs','r')
	except IOError:
		print 'The Master_accs file from the bitbucket repository is missing!'
		print 'Please download the file here: https://bitbucket.org/AdrianReich/phylogenetic-analysis-of-echinoderms/src'
		sys.exit()
	else:
		AccsRead.close()
		CountedLines=FileLen('Master_accs')
		if CountedLines!=1664288:	#known number of lines in uploaded file
			print 'This version of the Master_accs file is incorrect!'
			print 'It should have 1664288 lines, this one has:',str(CountedLines)
			print 'Please re-download the file here: https://bitbucket.org/AdrianReich/phylogenetic-analysis-of-echinoderms/src'
			sys.exit()	#End Master_accs presence verification and file length check
	#Begin TSA file verification	
	
	Accession = sys.argv[1]
	print 'Testing integrity of TSA fasta files.'
	#the CorrectAccessions dictionary has the key of valid accessions and the value is the number of transcripts in the files, temporarily, the value 'all' has a value of 1
	CorrectAccessions= {'GAVR':92455, 'GAVC':108540, 'GAVP':137152, 'GAWB':76838, 'GAVK':118293, 'GAVJ':63290, 'GAVS':85059, 'GAVI':118841, 'GAVQ':84378, 'GAVO':30606, 'GAVN':37111, 'GAVF':96976, 'GAVE':119578, 'GAUU':81466, 'GAUT':58272, 'GAUS':68714, 'GAUR':90621, 'GAUQ':111491, 'GAZO':39222, 'GAZP':45384, 'all':1}
	if Accession not in CorrectAccessions:	#check for correct input
		print 'You have entered an incorrect Accession code! Acceptable entries:'
		print str(CorrectAccessions.keys()).strip('[]')
		sys.exit()
	else:
		if Accession=='all':
			del CorrectAccessions[Accession]	#annotate all transcriptomes, remove the temporary 'all' placeholder in list of acceptable entries
		else:
			CorrectAccessions={Accession: CorrectAccessions[Accession]}	#annotate a single transcriptome, replace the entire dictionary with the only one they are interested in
		ExitTotal=False
		for Accession in CorrectAccessions:	#test if the transcriptomes to be processed can be opened
			CurrExit=FileOpen(Accession+'01.1.fsa_nt')
			if CurrExit==True:	#if any of the files can not be opened, stop the script after testing the rest of them (if any)
				ExitTotal=True
		if ExitTotal:	#once loop complete, exit if file(s) could not be opened
			sys.exit()
		for Accession in CorrectAccessions:	#test if the transcriptomes to be processed are complete
			CurrExit=FastaSeqCount(Accession+'01.1.fsa_nt', CorrectAccessions[Accession])
			if CurrExit==True:	#if any of the files are incomplete, stop the script after testing the rest of them (if any)
				ExitTotal=True
			else:
				print Accession+'01.1.fsa_nt is complete.'
		if ExitTotal:	#once loop complete, exit if file(s) are incomplete
			sys.exit()
	
	print 'All files appear to be correct, load Master_accs data.'	
	AccsRead = open('Master_accs','r')
	AccsDict={}	#key is NCBI accession number of sequence, value is list of [Reich_et_al_ID, Length, OvaryFPKM, SequenceName, SwissProtID, BlastMatch]
	AccsLine=AccsRead.readline().strip()	#removes header line in accs file
	AccsLine=AccsRead.readline().strip()
	LineCount=0
	TotalLineCount=1664288
	TwentyPercentTotal=int(TotalLineCount/20)
	PercentDone=20
	while AccsLine!='':
		LineCount+=1
		AccsFields=AccsLine.split('\t')
		AccsDict[AccsFields[0]]=AccsFields[1:]
		if LineCount==(TwentyPercentTotal*int(str(PercentDone)[0])):	#20% of the file is equal to TwentyPercentTotal, multiply by the ten's digit of PercentDone in order to calculate 40,60,etc
			print str(PercentDone)+'% of the accs file has been read'
			PercentDone+=20
		AccsLine=AccsRead.readline().strip()
	AccsRead.close()
	print 'Master_accs read complete.'
	
	for Accession in CorrectAccessions:	#begin annotations of TSA files 
		FastaFileName=Accession+'01.1.fsa_nt'
		print 'Begin annotation of',FastaFileName
		FastaRead = open(FastaFileName,'r')
		AnnotFastOut=open(Accession+'01.1.annot.fasta','w')
		AnnotSeqCount=0
		TotalSeqCount=0
		FaLine=FastaRead.readline().strip()
		while FaLine != '':
			if FaLine[0]=='>':
				TotalSeqCount+=1
				FaFields=FaLine.split(' ')
				NCBIdata=FaFields[0]	#include '>'
				Genus=FaFields[2]
				Species=FaFields[3]
				GenusSpecies=Genus+'_'+Species
				if Accession == 'GAVP' or Accession == 'GAVC':	#These two accessions have an extra field in the fasta title
					REALidFromTSA=FaFields[5]
				else:
					REALidFromTSA=FaFields[4]
				TSA_Accession=NCBIdata.split('|')[3].split('.')[0]
				if TSA_Accession not in AccsDict:	#means a sequence in TSA file is not found in Master_accs = problem
					print 'Missing accession from Master_accs!'
					print TSA_Accession
					sys.exit()
				else:	#everything should go here
					AccsFields=AccsDict[TSA_Accession]
					REALid=AccsFields[0]	#REALid = Reich Et AL id
					Length=AccsFields[1]
					FPKM=AccsFields[2]	#abundance in ovary tissue
					SeqName=AccsFields[3]	#SwissProt name
					if SeqName!='No Annotation':
						AnnotSeqCount+=1
					SwissProt=AccsFields[4]	#SwissProt ID
					Blast=AccsFields[5]	#blast hit of this sequence to SwissProt sequence
					if REALidFromTSA!=REALid:	#means the master_accs file is possibly wrong
						print 'Mismatched accessions between Master_accs and TSA fasta file!'
						print REALidFromTSA, REALid
						sys.exit()
					else:	#again, everything should go here
						OutString=[NCBIdata, GenusSpecies]
						if Bool_REALid:
							OutString.append(REALid)
						if Bool_Length:
							OutString.append(Length)
						if Bool_FPKM:
							OutString.append(FPKM)
						if Bool_SwissProt:
							OutString.append(SwissProt)
						if Bool_Blast:
							OutString.append(Blast)
						OutString.append(SeqName)
						OutString=' '.join(OutString)
						AnnotFastOut.write(OutString+'\n')
			else:	#write out fasta sequence
				AnnotFastOut.write(FaLine+'\n')
			FaLine=FastaRead.readline().strip()
		AnnotFastOut.close()
		print FastaFileName, 'annotation complete:',str(AnnotSeqCount),'anotated sequences out of a total of',str(TotalSeqCount),'sequences.'
